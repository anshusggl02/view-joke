function jokefunction() {

    const img1 = document.createElement("img");
    img1.src = "https://assets.chucknorris.host/img/avatar/chuck-norris.png";
    img1.setAttribute("id", "image");
    document.getElementById("element1").appendChild(img1);

    const btn = document.createElement("button");
    btn.innerHTML = "view joke";
    btn.setAttribute("id", "button");

    document.getElementById("element3").appendChild(btn);
    btn.addEventListener("click", fetchJokes);


}



function fetchJokes() {
    fetch("https://api.chucknorris.io/jokes/random")
        .then(response => response.json())
        .then(data => {
            let checkElement = document.getElementById("dataElement");
            if (Boolean(checkElement)) {
                replaceJoke(data);
            } else {
                const dataElement = document.createElement("p");
                dataElement.setAttribute("id", "dataElement");
                dataElement.innerHTML = data.value;
                document.getElementById("element2").appendChild(dataElement);
                console.log(data);
            }

        });
}

function replaceJoke(data) {
    document.getElementById("dataElement").innerHTML = data.value;
}



jokefunction();